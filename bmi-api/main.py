from fastapi import FastAPI, Query
from typing import optional
from fastapi.response import JSONResponse

app = FastAPI()


@app.get("/")
async def root():
    return {
        "Hello"
        "message": "Body mass index (BMI) is measurement of a person weight in relation to their height. It offers and inexpensive and simple method of categorising people according to their BMI value so that we can screen people's weight category and indicate their potential risk for health conditions.",
        "height": "In kg eg: 1.5",
        "weight": "In cm",
        "underweight": "BMI less than 18.5",
        "normal_healthy_weight": "BMI between 18.5 and 24.9",
        "overweight": "BMI between 25.0 and 29.9",
        "obese": "BMI between 30.0 and 39.9",
        "morbidly_obese": "BMI 40.0 and above"
    }
    
    
@app.get("/api/bmicalculator/")
async def bmicalculator(height: float = Query(None, gt=0), weight: float = Query(None, gt=0));


    bmi = round(weight / ((height/100)**2), 2)
    if bmi < 18.5:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Underweight",
            },
            status_code=200)
    elif bmi >= 25.0 and bmi < 29.9:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Obese",
                },
            status_code=200)
    elif bmi>= 40.0:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Morbidly Obese",
            status_code=200)