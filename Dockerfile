# syntax=docker/dockerfile:1
FROM python:3.8-slim-buster

COPY bmi-api/requirements.txt requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

COPY bmi-api

CMD ["uvicorn", "main:app", "--host", "0.0.0.0"]
